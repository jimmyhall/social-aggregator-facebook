var AWS = require('aws-sdk');
var DOC = require('dynamodb-doc');
var dynamo = new DOC.DynamoDB();

var FB = require('fb');
FB.options({'appSecret': 'xxxx', 'accessToken': 'xxxx|xxxx'});

var networks = ["Facebook"];

//Callback functions
var error = function (err, response, body) {
    console.log('ERROR [%s]', err);
};
var success = function (data) {
    console.log('Data [%s]', data);
};

var cb = function(err, data) {
    if(err) {
        console.log(err);
    } else {

    }
};

var messageOutput = "Results\n";

exports.handler = function(event, context) {
    var cb = function(err, data) {
        if(err) {
            console.log('error on RetrieveSocialFeed: ',err);
            context.done('Unable to retrieve Social Site Data', null);
        } else {
            data.Items.forEach(function(item) {
                if (item && item.SiteId) {
                    networks.forEach(function (elem) {
                        if (item[[elem] + "Status"]) {
                            loopThroughNetworks(elem, item[[elem] + "Id"], item.SiteId);
                        }
                    });
                }
            });
        }
    };

    var params = {
        TableName : "SocialSiteSettings",
        FilterExpression: "SiteStatus = :siteStatus",
        ExpressionAttributeValues: {
            ":siteStatus": true,
        }
    };

    dynamo.scan(params, cb);
};

function loopThroughNetworks(network, networkId, siteId) {
    // call the correct function for the network in question returning a count of items found
    eval([network] + "Feed")(networkId, siteId);
}

function FacebookFeed(networkId, siteId) {

    FB.api(networkId + '/posts?fields=created_time,message,id,picture,full_picture', function (data) {
        if(!data || data.error) {
            console.log(!data ? 'error occurred' : data.error);
            return;
        }

        var posts = data['data'];

        console.log("\nFacebook: " + networkId + " - " + posts.length);

        for (i in posts) {

            var DateCreated = Math.round(+new Date(Date.parse(posts[i]['created_time'].replace(/( \+)/, ' UTC$1')))/1000);

            if (typeof posts[i]['message'] !== 'undefined') {
                var item = {
                    ItemId: siteId + posts[i]['id'].split('_')[1],
                    SiteId: Number(siteId),
                    FeedType: "Facebook",
                    DateCreated: DateCreated,
                    Title: posts[i]['message'] + '<span class="f-image"><img src="' + posts[i]['full_picture'] + '"></span>',
                    Active: true
                };
                dynamo.putItem({TableName:"SocialFeedsData", Item:item}, cb);
            }
        }
    });

}
